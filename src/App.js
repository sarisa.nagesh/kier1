// import React from 'react';
// import './App.css';
// import Landing from './pages/Landing';
// import Footer from './pages/Footer';
// import nav1 from './pages/nav1';
// import billLeft from './pages/billLeft';
// function App() {
//   return (
//     <div>
//       <Landing />
//       <Footer />
//       <nav1 />
//       <billLeft />
//     </div>
//   );
// }
// export default App;

import React, { Component } from 'react'; 
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom'; 
import Landing from './pages/Landing';
import Footer from './pages/Footer';
import loginpage from './pages/loginpage';
import password from './pages/passwordform';
import nav1 from './pages/nav1';
import billLeft from './pages/billLeft';
import Freelancer from './pages/Page14';
import Billing from './pages/Page15';
import Posting from './pages/Page18';
import page6r from './pages/page-6r';
import page13 from './pages/page-13';
import './App.css'; 
import page9r from './pages/page-9r';
import page10r from './pages/page-10r';
import page17r from './pages/page-17r';
import page21r from './pages/page-21r';
import page22r from './pages/page-22r';
import page23r from './pages/page-23r';
import page24r from './pages/page-24r';
import lastl from './pages/last-left';

class App extends Component { 
render() { 
	return ( 
	<Router> 
			<Switch> 
			<Route exact path='/' component={Landing}></Route> 
			<Route exact path='/footer' component={Footer}></Route> 
			<Route exact path='/login' component={loginpage}></Route> 
			<Route exact path='/password' component={password}></Route>
			<Route exact path='/nav' component={nav1}></Route> 
			<Route exact path='/billLeft' component={billLeft}></Route>
			<Route exact path='/page9r' component={page9r}></Route>
			<Route exact path='/page10r' component={page10r}></Route>
			<Route exact path='/page17r' component={page17r}></Route>
			<Route exact path='/page21r' component={page21r}></Route>
			<Route exact path='/page22r' component={page22r}></Route>
			<Route exact path='/page23r' component={page23r}></Route>
			<Route exact path='/page24r' component={page24r}></Route>
			<Route exact path='/page14' component={Freelancer}></Route>
			<Route exact path='/page15' component={Billing}></Route>
			<Route exact path='/page18' component={Posting}></Route>
			<Route exact path='/page6r' component={page6r}></Route>
			<Route exact path='/page13' component={page13}></Route>
			<Route exact path='/lastl' component={lastl}></Route>
			
			</Switch> 
		
	</Router> 
); 
} 
} 

export default App; 

import react from 'react';
import "./page-21r.css";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";


class page21r extends react.Component {

  render() {
    return (
<>
  <div className="last-21-right-section pt-5 pb-5">
    {/* <div className="container"> */}
      {/* <div className="row justify-content-center"> */}
        <div className="col-12 col-md-7">
          <div className="shadow last-21-right-item-card mb-3">
            <div className="head-card p-3">
              <div className="d-flex flex-row">
                <h1 className="head-card-heading">Location</h1>
                <h1 className="head-card-heading-1">Step 5 Of 8</h1>
              </div>
            </div>
            <div className="body-card p-3">
              <h1 className="body-card-heading">Talent Location</h1>
              <div className="d-flex flex-row">
                <div className="location-card shadow text-center p-3 m-3">
                  <i className="fas fa-street-view" />
                  <h1 className="body-card-heading">Talent Location</h1>
                  <h1 className="body-card-heading-1">Only talent in my local</h1>
                  <h1 className="body-card-heading-1">area can submit a proposal</h1>
                </div>
                <div className="location-card shadow text-center p-3 m-3">
                  <i className="fas fa-globe" style={{color: '#fe885d'}} />
                  <h1 className="body-card-heading">worldwide</h1>
                  <h1 className="body-card-heading-1">talent in any location can</h1>
                  <h1 className="body-card-heading-1">submit proposals</h1>
                </div>
              </div>
              <h1 className="body-card-heading mt-3">Region Or Country Prefernce (Optional)</h1>
              <div className="input-group shadow mt-3">
                <div className="input-group-prepend">
                  <button type="button" className="btn btn-outline-secondary" style={{borderWidth: 0, color: '#fe885d'}}><i className="fas fa-map-marker-alt" /></button>
                </div>
                <input type="text" className="form-control" style={{borderWidth: 0}} placeholder="Add Regions Or Countries" aria-label="Add Regions Or Countries" />
                <div className="input-group-prepend">
                  <button className="btn btn-outline-secondary dropdown-toggle" style={{borderWidth: 0, color: '#fe885d'}} type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" />
                </div>
              </div>
              <h1 className="body-card-heading-2 mt-3">The Location Preferences Will Be Displayed To Freelancer And Agencies, But Anyone Can Submit Proposals.</h1>
              <div className="d-flex flex-row justify-content-center mt-5 mb-3">
                <button className="btn btn-banner ml-3" href="#" id="location-card-Item2">Back</button>
                <button className="btn btn-banner ml-3" href="#" id="location-card-Item3">Next</button>
              </div>
            </div>
          </div>
        </div>
      {/* </div> */}
    {/* </div> */}
  </div>
</>
)
}
}
export default page21r;
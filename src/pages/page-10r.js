import react from 'react';
import "./page-10r.css";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";


class page10r extends react.Component {

  render() {
    return (
        <div className="last-23-right-section pt-5 pb-5">
    {/* <div className="container"> */}
      {/* <div className="row justify-content-center"> */}
        <div className="col-12 col-md-6 col-lg-7">
          <div className="shadow last-23-right-item-card mb-3">
            <div className="head-card p-3">
              <div className="d-flex flex-row">
                <h1 className="head-card-heading">Billing Methods</h1>
                <Link className="ml-auto" href="#"><i className="fas fa-plus shadow" id="head-card-icon-1" /></Link>
                <h1 className="head-card-heading-2">Add Method</h1>
              </div>
            </div>
            <div className="body-card p-3">
              <div className="d-flex flex-row">
                <h1 className="body-card-heading">Primary</h1>
                <Link className="ml-auto" href="#"><i className="fas fa-trash shadow" id="body-card-icon-1" /></Link>
                <Link className="ic ml-3" href="#"><i className="fas fa-pencil-alt shadow" id="body-card-icon-2" /></Link>
              </div>
              <div className="d-flex flex-row">
                <i className="fab fa-cc-visa" />                               
                <p className="body-card-heading ml-3">Visa Ending In 0000</p>
              </div> 
            </div>
          </div>
        </div>
      </div>
    // </div>
//   </div>
    )
  }
}
export default page10r;
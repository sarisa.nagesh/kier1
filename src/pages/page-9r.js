import react from 'react';
import "./page-9r.css";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";


class page9r extends react.Component {

  render() {
    return (
      <>
      <div className="last-9-right-section pt-5">
        {/* <div className="container"> */}
          {/* <div className="row justify-content-center"> */}
            <div className="col-12 col-md-6 col-lg-7">
              <div className="shadow last-9-right-item-card mb-3">
                <div className="head-card p-3">
                  <div className="d-flex flex-row">
                    <h1 className="head-card-heading">Account</h1>
                    <Link className="ml-auto" href="#"><i className="fas fa-pencil-alt shadow" id="head-card-icon-1" /></Link>
                  </div>
                </div>
                <div className="body-card p-3">
                  <div className="m-3">
                    <h1 className="body-card-heading">User Id</h1>
                    <h1 className="body-card-heading-1 mb-3">Lorieum Epsium</h1>
                  </div>
                  <div className="m-3">
                    <h1 className="body-card-heading">Name</h1>
                    <h1 className="body-card-heading-1 mb-3">Lorieum Epsium</h1>
                  </div>
                  <div className="m-3">
                    <h1 className="body-card-heading">Email</h1>
                    <h1 className="body-card-heading-1 mb-3">Lorieum Epsium</h1>
                  </div>
                  <Link className="closeacc ml-auto" href="#">Close My Account</Link>
                </div>
              </div>
            </div>
          </div>
        {/* </div> */}
      {/* </div> */}
      <div className="last-9-right-section pt-3 pb-5">
        {/* <div className="container"> */}
          {/* <div className="row justify-content-center"> */}
            <div className="col-12 col-md-6 col-lg-7">
              <div className="shadow last-9-right-item-card mb-3">
                <div className="head-card p-3">
                  <div className="d-flex flex-row">
                    <h1 className="head-card-heading">Account</h1>
                    <Link className="ml-auto" href="#"><i className="fas fa-pencil-alt shadow" id="head-card-icon-1" /></Link>
                  </div>
                </div>
                <div className="body-card p-3">
                  <div className="m-3">
                    <h1 className="body-card-heading">User Id</h1>
                    <h1 className="body-card-heading-1 mb-3">Lorieum Epsium</h1>
                  </div>
                  <div className="m-3">
                    <h1 className="body-card-heading">Name</h1>
                    <h1 className="body-card-heading-1 mb-3">Lorieum Epsium</h1>
                  </div>
                  <div className="m-3">
                    <h1 className="body-card-heading">Email</h1>
                    <h1 className="body-card-heading-1 mb-3">Lorieum Epsium</h1>
                  </div>
                  <Link className="closeacc ml-auto" href="#">Close My Account</Link>
                </div>
              </div>
            </div>
          </div>
        {/* </div> */}
      {/* </div> */}
  </>
    )
  }
}
export default page9r;
import react from 'react';
import "./page-17r.css";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";


class page17r extends react.Component {

  render() {
    return (
        <>
  <div className="last-17-right-section pt-5 pb-5">
    {/* <div className="container"> */}
      {/* <div className="row justify-content-center"> */}
        <div className="col-12 col-md-6 col-lg-7">
          <div className="shadow last-17-right-item-card mb-3">
            <div className="head-card p-3">
              <div className="d-flex flex-row">
                <h1 className="head-card-heading">Expertise</h1>
                <h1 className="head-card-heading-1">Step 1 Of 8</h1>
              </div>
            </div>
            <div className="body-card p-3">
              <h1 className="head-card-heading">Enter The Name Of Your Job Post</h1>
              <textarea id="subject" name="subject" placeholder defaultValue={""} />
            </div>
          </div>
          <div className="ml-5">
            <h1 className="body-card-heading mt-3">Here Are Some Good Examples :</h1>
            <ul className="bullet">
              <li>Need A Plumber For My Company</li>
              <li>Need A Plumber For My Company</li>
              <li>Need A Plumber For My Company</li>
            </ul>
          </div>
          <div className="last-17-right-item-card mb-3">
            <div className="head-card p-3">
              <div className="d-flex flex-row">
                <h1 className="head-card-heading">Job Category</h1>
              </div>
            </div>
          </div>
          <div className="input-group mt-3">
            <input type="text" className="form-control" style={{borderColor: '#a3a1a1'}} placeholder="Select Category And Subcategory" aria-label="Select Category And Subcategory" />
            <div className="input-group-prepend">
              <button className="btn btn-outline-secondary dropdown-toggle" style={{borderLeft: 0, borderColor: '#a3a1a1', color: '#fe885d'}} type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" />
            </div>
          </div>
          <div className="d-flex flex-row mt-5 mb-3">
            <button className="btn btn-banner ml-3" href="#" id="location-card-Item2">Exit</button>
            <button className="btn btn-banner ml-3" href="#" id="location-card-Item3">Next</button>
          </div>
        </div>
      {/* </div> */}
    {/* </div> */}
  </div>
</>

    )
  }
}
export default page17r;
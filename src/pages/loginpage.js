import react from 'react';
import "./loginpage.css";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";


class loginpage extends react.Component {

  render() {
    return (
<>
        <div className="login-section pt-3 pb-3">
            <div className="col-12">
                <Link className="main-logo">
                    Logo
                </Link>
            </div>
            <div className="row m-5">
                <div className="col-12 col-md-6">
                        <div className="d-flex flex-column pt-5">
                            <h1 className="main-head pt-5">Welcome ..</h1>
                            <h2 className="story">Hire The Best Freelancers Online For</h2>
                            <h2 className="story">Any Local Job</h2>
                        </div>
                </div>
                <div className="col-12 col-md-5">   
                        <div className="d-flex flex-column shadow justify-content-center p-5">
                            <h2 className="box-head mt-3 mb-3">log in and get to work</h2>
                            <input id="email" type="text" placeholder=" username or email " />
                            <button className="btn btn-banner mt-3" href="#" id="bannerItem">Continue</button>
                            <h2 className="line mt-3">--------------or---------------</h2>
                            <button className="btn btn-banner1 mt-3" href="#" id="bannerItem2">
                                <i className="fa fa-google fa-fw" ></i>&nbsp;&nbsp;&nbsp;Sign In with Google
                            </button>
                            <button className="btn btn-banner2 mt-3" href="#" id="bannerItem2">
                                <i className="fa fa-apple" aria-hidden="true"></i>Sign In with Apple
                            </button>
                            <button className="btn btn-banner3 mt-3" href="#" id="bannerItem3">
                                 <i className="fa fa-facebook" aria-hidden="true"></i>Sign In with Facebook
                            </button>
                            <button className="btn btn-banner4 mt-3" href="#" id="bannerItem3">
                                 <i className="fa fa-linkedin-square" aria-hidden="true"></i>Sign In
                                with Linkedin
                            </button>
                            <div className="d-flex flex-row justify-content-center">
                                <h5 className="signup-head mt-3 mb-3">New To Kierkou ?</h5>
                                <Link className="signup m-3">Sign up</Link>
                            </div>
                        </div>
                </div>
            </div>
            
        </div>


</>


)
}
}
export default loginpage;
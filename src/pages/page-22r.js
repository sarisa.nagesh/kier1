import react from 'react';
import "./page-22r.css";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";


class page22r extends react.Component {

  render() {
    return (
<>
<div className="last-22-right-section pt-5 pb-5">
    {/* <div className="container"> */}
      {/* <div className="row justify-content-center"> */}
        <div className="col-12 col-md-6 col-lg-7">
          <div className="shadow last-22-right-item-card mb-3">
            <div className="head-card p-3">
              <div className="d-flex flex-row">
                <h1 className="head-card-heading">Review And Post</h1>
                <button className="btn btn-banner ml-auto" href="#" id="head-card-Item1">Post Job Now</button>
              </div>
            </div>
            <div className="body-card p-3">
              <div className="d-flex flex-row">
                <h1 className="body-card-heading">Title</h1>
                <Link className="ml-auto" href="#"><i className="fas fa-pencil-alt shadow" id="body-card-icon-1" /></Link>
              </div>
              <div className="ml-3">
                <h1 className="body-card-heading mt-3">Title</h1>
                <h1 className="body-card-heading-1">Need A Cross Platform</h1>
                <h1 className="body-card-heading mt-3">Job Category</h1>
                <h1 className="body-card-heading-1">Mobile App Development</h1>
              </div>
            </div>
          </div>
          <div className="shadow last-22-right-item-card mb-3">
            <div className="body-card p-3">
              <div className="d-flex flex-row">
                <h1 className="body-card-heading">Description</h1>
                <Link className="ml-auto" href="#"><i className="fas fa-pencil-alt shadow" id="body-card-icon-1" /></Link>
              </div>
              <div className="ml-3">
                <h1 className="body-card-heading mt-3">Description</h1>
                <h1 className="body-card-heading-1">I'm Looking For An Experienced App Developer</h1>
              </div>
            </div>
          </div>
          <div className="shadow last-22-right-item-card mb-3">
            <div className="body-card p-3">
              <div className="d-flex flex-row">
                <h1 className="body-card-heading">Details</h1>
                <Link className="ml-auto" href="#"><i className="fas fa-pencil-alt shadow" id="body-card-icon-1" /></Link>
              </div>
              <h1 className="body-card-heading mt-3">Type Of Project</h1>
              <h1 className="body-card-heading-1">Ongoing Project</h1>
              <hr className="hr-line" />
              <h1 className="body-card-heading mt-3" style={{fontSize: 20}}>Screening Questions</h1>
              <h1 className="body-card-heading mt-3">Require Cover Letter</h1>
              <h1 className="body-card-heading-1">Yes</h1>
            </div>
          </div>
          <div className="shadow last-22-right-item-card mb-3">
            <div className="body-card p-3">
              <div className="d-flex flex-row">
                <h1 className="body-card-heading">Expertise</h1>
                <Link className="ml-auto" href="#"><i className="fas fa-pencil-alt shadow" id="body-card-icon-1" /></Link>
              </div>
              <h1 className="body-card-heading mt-3">The Quick.</h1>
              <button className="quick">The Quick.</button>
              <h1 className="body-card-heading mt-3">The Quick.</h1>
              <div className="group-1 d-flex flex-row mb-3">
                <button className="quick">The Quick.</button>
                <button className="quick">The Quick.</button>
              </div>
              <hr className="hr-line" />
              <h1 className="body-card-heading mt-3">Require Cover Letter</h1>
              <h1 className="body-card-heading-1">Yes</h1>
            </div>
          </div>
          <div className="shadow last-22-right-item-card mb-3">
            <div className="body-card p-3">
              <div className="d-flex flex-row">
                <h1 className="body-card-heading">Visibility</h1>
                <Link className="ml-auto" href="#"><i className="fas fa-pencil-alt shadow" id="body-card-icon-1" /></Link>
              </div>
              <h1 className="body-card-heading mt-3">Job Posting Visibility</h1>
              <h1 className="body-card-heading-1">Freelancers And Agencies Using Kierkou And Public Engines Can Find This Job</h1>
              <h1 className="body-card-heading mt-3">Talent Preferences</h1>
              <h1 className="body-card-heading">Talent Type</h1>
              <div className="d-flex flex-row">
                <h1 className="body-card-heading-1">No Preferences ...</h1>
                <Link className="body-card-more" href="#">More</Link>
              </div>
            </div>
          </div>
          <div className="shadow last-22-right-item-card mb-3">
            <div className="body-card p-3">
              <div className="d-flex flex-row">
                <h1 className="body-card-heading">Budget</h1>
                <Link className="ml-auto" href="#"><i className="fas fa-pencil-alt shadow" id="body-card-icon-1" /></Link>
              </div>
              <div className="d-flex flex-row">
                <div className="d-flex flex-column">
                  <h1 className="body-card-heading mt-3">Hourly Or Fixed-Price</h1>
                  <h1 className="body-card-heading-1">Pay A Fixed Price</h1>
                </div>
                <div className="d-flex flex-column ml-auto">
                  <h1 className="body-card-heading mt-3">Project Duration</h1>
                  <h1 className="body-card-heading-1">More Than 6 Months</h1>
                </div>
              </div>
              <h1 className="body-card-heading mt-3">Budget</h1>
              <h1 className="body-card-heading-1">$3.000</h1>
            </div>
          </div>
          <div className="shadow last-22-right-item-card mb-3">
            <div className="body-card p-3">
              <div className="d-flex flex-row">
                <h1 className="body-card-heading">Location</h1>
                <Link className="ml-auto" href="#"><i className="fas fa-pencil-alt shadow" id="body-card-icon-1" /></Link>
              </div>
              <h1 className="body-card-heading mt-3">Freelancer Location</h1>
              <h1 className="body-card-heading-1">Worldwide, Freelancers In Any Location Can Submit Proposals.</h1>
            </div>
          </div>
          <div className="shadow last-22-right-item-card mb-3">
            <div className="body-card p-3">
              <div className="d-flex flex-row">
                <i className="far fa-gem" id="daimond" />
                <h1 className="body-card-heading ml-3">Boost Y Boost Your Job's Visibility</h1>
                <Link className="ml-auto" href="#"><i className="fas fa-pencil-alt shadow" id="body-card-icon-1" /></Link>
              </div>
              <h1 className="body-card-heading mt-3" style={{fontSize: 20}}>Boost Your Job's Visibility</h1>
              <h1 className="body-card-heading-1 mt-3">We'll Feature Your Job And Top Talent Will Recieve Discounts To Work On Your Projects.</h1>
              <h1 className="body-card-heading-1" style={{fontSize: 10}}>Tell Me How I Can Reach More Freelancers And Agencies And Hire In Less Time.</h1>
            </div>
          </div>
          <div className="shadow last-22-right-item-card mb-3">
            <div className="body-card p-3">
              <div className="d-flex flex-row">
                <h1 className="body-card-heading">Share With Coworkers</h1>
                <Link className="ml-auto" href="#"><i className="fas fa-plus shadow" id="body-card-icon-1" /></Link>
              </div>
              <h1 className="body-card-heading-1 mt-3">Invite Coworkers To Help You Find, Interview, And Evaluate Freelancers.</h1>
              <div className="d-flex flex-row mt-4">
                <Link href="#"><i className="fas fa-pencil-alt shadow" id="body-card-icon-1" /></Link>
                <h1 className="body-card-heading-1 ml-3">Share With Coworkers</h1>
              </div>
            </div>
          </div>
          <div className="d-flex flex-row">
            <button className="btn btn-banner ml-3" href="#" id="head-card-Item2">Post Job Now</button>
            <button className="btn btn-banner ml-3" href="#" id="head-card-Item3">Save &amp; Exit</button>
          </div>
        </div>
      {/* </div> */}
    {/* </div> */}
  </div>
</>
)
}
}
export default page22r;
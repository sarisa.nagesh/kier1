import react from 'react';
import "./page-13.css";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";


class page13 extends react.Component {

  render() {
    return (
<>
<div className="page-13-head-section pt-1 pb-1">
    {/* <div className="container"> */}
      {/* <div className="row"> */}
        <div className="col-12 mt-3">
          <div className="d-flex flex-row">
            <h1 className="logo-head mt-3">Logo.Help</h1>
            <p className="logo-para ml-auto mt-3">My Support Request</p>
            <img className="user-image" src="https://lenstax.com/auth/app-assets/images/profile/user-uploads/user-04.jpg" />
          </div>
          <h1 className="logo-head mt-3 mb-3 text-center">What Do You Need Help With ?</h1>
          <div className="mb-5">
            <div className="input-group m-auto shadow">
              <div className="input-group-prepend">
                <button className="btn btn-outline-secondary" type="button" style={{borderWidth: 0, backgroundColor: 'white', color: '#fe885d', borderTopLeftRadius: 25, borderBottomLeftRadius: 25}} id="button-addon1"><i className="fas fa-search" /></button>
              </div>
              <input type="text" className="form-control" placeholder="loreum espium" style={{borderWidth: 0}} aria-label aria-describedby="button-addon1" />
              <div className="input-group-prepend">
                <button className="btn btn-outline-secondary" type="button" style={{borderWidth: 0, backgroundColor: '#fe885d', color: 'white', width: 100, borderTopRightRadius: 25, borderBottomRightRadius: 25}} id="button-addon1"><i className="fas fa-search" /></button>
              </div>
            </div>
          </div>
        </div>
      {/* </div> */}
    {/* </div> */}
  </div>
  <div className="page-13-body-title-section">
    {/* <div className="container"> */}
      {/* <div className="row"> */}
        <div className="col-12">
          <div className="title-card">
            <div className="d-flex flex-row justify-content-center">
              <Link className="title" href="#">Client</Link>
              <Link className="title ml-5" href="#">Freelancer</Link>
            </div>
            <hr />
          </div>
        {/* </div> */}
      {/* </div> */}
    </div>
    {/* <div className="container"> */}
      <div className="row">
        <div className="col-12">
          <h1 className="middle-head">Popular Topics</h1>
        </div>
        <div className="col-12 col-md-6 col-lg-3 p-3">
          <div className="middle-card">
            <div className="d-flex flex-column text-center">
              <Link className="middle p-3" href="#">Get Started</Link>
              <p className="middle-para p-3">How It Works , Getting Started Fees&amp; Protection</p>
            </div>
          </div>
        </div>
        <div className="col-12 col-md-6 col-lg-3 p-3">
          <div className="middle-card">
            <div className="d-flex flex-column text-center">
              <Link className="middle p-3" href="#">Build Y Build Your Profile</Link>
              <p className="middle-para p-3">Profile Settings,Edit Profile, Freelancer Programs, Stats</p>
            </div>
          </div>
        </div>
        <div className="col-12 col-md-6 col-lg-3 p-3">
          <div className="middle-card">
            <div className="d-flex flex-column text-center">
              <Link className="middle p-3" href="#">Find A Project</Link>
              <p className="middle-para p-3">Search Send Proposals,
                Interview, Accept Offers</p>
            </div>
          </div>
        </div>
        <div className="col-12 col-md-6 col-lg-3 p-3">
          <div className="middle-card">
            <div className="d-flex flex-column text-center">
              <Link className="middle p-3" href="#">Start Working</Link>
              <p className="middle-para p-3">Messages , Work Diary,
                Contracts ,Feedback</p>
            </div>
          </div>
        </div>
        <div className="col-12 col-md-6 col-lg-3 mt-2 p-3">
          <div className="middle-card">
            <div className="d-flex flex-column text-center">
              <Link className="middle p-3" href="#">Get Paid</Link>
              <p className="middle-para p-3">Get Paid , Payment Options,
                Reports,Earnings,Taxes</p>
            </div>
          </div>
        </div>
        <div className="col-12 col-md-6 col-lg-3 mt-2 p-3">
          <div className="middle-card">
            <div className="d-flex flex-column text-center">
              <Link className="middle p-3" href="#">Payment Issues</Link>
              <p className="middle-para p-3">Payment Schedule.
                Troubleshooting,Disputes</p>
            </div>
          </div>
        </div>
        <div className="col-12 col-md-6 col-lg-3 mt-2 p-3">
          <div className="middle-card">
            <div className="d-flex flex-column text-center">
              <Link className="middle p-3" href="#">Account</Link>
              <p className="middle-para p-3">Account Settings,Service
                Options,Identify Verification</p>
            </div>
          </div>
        </div>
        <div className="col-12 col-md-6 col-lg-3 mt-2 p-3">
          <div className="middle-card">
            <div className="d-flex flex-column text-center">
              <Link className="middle p-3" href="#">Apps</Link>
              <p className="middle-para p-3">Mobile App, Desktop App,
                Time Tracker</p>
            </div>
          </div>
        </div>
      </div>
    {/* </div> */}
    {/* <div className="container"> */}
      {/* <div className="row"> */}
        <div className="col-12">
          <div className="foot-card shadow mt-3 mb-5 p-3">
            <div className="d-flex flex-row">
              <div className="d-flex flex-column">
                <Link className="foot" href="#">Need More Help ?</Link>
                <h1 className="foot-head">contact us</h1>
                <button className="btn btn-banner" href="#" id="foot-card-Item1">Get Help</button>
              </div>
              <img className="img-1 ml-5" src="https://thumbs.dreamstime.com/b/carrier-pigeon-letter-16527674.jpg" />
              <img className="img-1" src="https://img2.pngio.com/dotted-line-clipart-png-dashed-lines-png-500_280.jpg" />
              <img className="img-1" src="https://www.abelhr.com/wp-content/uploads/2017/07/help.jpg" />
            </div>
          </div>
        </div>
      {/* </div> */}
    {/* </div> */}
    </div>
    </>
)
}
}
export default page13;
import React, {Component} from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import './nav1.css';
class nav1 extends Component {
    render() {
	    return (
		<>
        <nav className="navbar navbar-expand-lg navbar-light bg-white fixed-top pt-4">
    {/* <div className="container"> */}
      <Link className="navbar-brand" href="#">Logo</Link>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav" id="middle">
          <Link className="nav-link" id="navItem1" href="#HowItWorksSection">
            How It Works
          </Link>
          <Link className="nav-link ml-3" href="#BrowseJobsSection" id="navItem2">Browse Jobs</Link>
          <Link className="nav-link ml-3" href="#FindAFreelancerSection" id="navItem3">Find A Freelancer</Link>
        </div>
        <div className="navbar-nav ml-auto">
          <div className="input-group">
            <div className="input-group-prepend">
              <button type="button" className="btn btn-outline-secondary" style={{borderWidth: 1, borderRight: 0, borderColor: '#fe885d', color: '#fe885d', borderTopLeftRadius: 25, borderBottomLeftRadius: 25}}><i className="fas fa-search" /></button>
              <button className="btn btn-outline-secondary dropdown-toggle" style={{borderLeft: 0, borderRight: 0, borderColor: '#fe885d', color: '#fe885d'}} type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" />
            </div>
            <input type="text" className="form-control" style={{borderWidth: 1, borderLeft: 0, borderColor: '#fe885d', borderTopRightRadius: 25, borderBottomRightRadius: 25}} placeholder="Find Freelancers" aria-label="Find Freelancers" />
          </div>
          <i className="fas fa-question-circle" id="navicon1" />
          <i className="far fa-comment-alt" id="navicon2" />
          <img className="nav1-image" alt="" src="https://lenstax.com/auth/app-assets/images/profile/user-uploads/user-04.jpg" />
          <Link className="nav1-username" href="#"> Haninaburass</Link>
          <button className="btn btn-outline-secondary dropdown-toggle" style={{borderWidth: 0, color: '#fe885d'}} type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" />
        </div>
      </div>
    {/* </div> */}
  </nav>
</>

	     );
    }
}
export default nav1;
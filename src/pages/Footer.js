import React, { Component } from "react";
import "./Footer.css";
// import 'bootstrap/dist/css/bootstrap.css';
class Footer extends Component {
  render() {
    return (
      <>
      {/* // <div className="row">
        // <div className="medium-12 columns"> */}
          <div className="ecommerce-footer-section pt-1 pb-1">
            {/* <div className="container"> */}
              <div className="row justify-content-center">
                <div className="col-6 col-lg-3 mt-3">
                  <h1 className="ecommerce-footer-section-heading mb-3">
                    Get to know us
                  </h1>
                  <ul className="ecommerce-footer-section-list">
                    <li className="ecommerce-footer-section-list-item">
                      About us
                    </li>
                    <li className="ecommerce-footer-section-list-item">
                      Career
                    </li>
                    <li className="ecommerce-footer-section-list-item">
                      Press Releases
                    </li>
                    <li className="ecommerce-footer-section-list-item">
                      Gift a smile
                    </li>
                  </ul>
                </div>
                <div className="col-6 col-lg-3 mt-3">
                  <h1 className="ecommerce-footer-section-heading mb-3">
                    Contact with Us
                  </h1>
                  <ul className="ecommerce-footer-section-list">
                    <li className="ecommerce-footer-section-list-item">
                      Facebook
                    </li>
                    <li className="ecommerce-footer-section-list-item">
                      Twitter
                    </li>
                    <li className="ecommerce-footer-section-list-item">
                      Instagram
                    </li>
                  </ul>
                </div>
                <div className="col-6 col-lg-3 mt-3">
                  <h1 className="ecommerce-footer-section-heading mb-3">
                    Let Us Help You
                  </h1>
                  <ul className="ecommerce-footer-section-list">
                    <li className="ecommerce-footer-section-list-item">
                      100% Purchase Protection
                    </li>
                    <li className="ecommerce-footer-section-list-item">
                      Your Account
                    </li>
                    <li className="ecommerce-footer-section-list-item">
                      Return Centre
                    </li>
                    <li className="ecommerce-footer-section-list-item">Help</li>
                  </ul>
                </div>
              </div>
            {/* </div> */}

            {/* <div className="container">
              <div className="row"> */}
                <div className="col-12 mt-3">
                  <div className="d-flex flex-row">
                    <p className="follow">Follow Us :</p>
                    <i className="fab fa-facebook-f" id="footicon1"></i>
                    <i className="fab fa-twitter" id="footicon1"></i>
                    <i className="fab fa-linkedin-in" id="footicon1"></i>
                    <i className="fab fa-youtube" id="footicon1"></i>
                    <i className="fas fa-phone-alt" id="footicon1"></i>
                  </div>
                </div>
              {/* </div>
            </div> */}
            {/* <div className="container">
              <div className="row"> */}
                <div className="col-12">
                  <div className="d-flex flex-row mb-3">
                    <img
                      className="appstore-img ml-auto d-none d-md-block"
                      alt=""
                      src="https://miro.medium.com/max/816/1*sSR4mrpijxoQrD7HKu8nDw.png"
                    />
                    <img
                      className="appstore-img d-block d-md-none"
                      alt=""
                      src="https://miro.medium.com/max/816/1*sSR4mrpijxoQrD7HKu8nDw.png"
                    />
                  </div>
                </div>
              {/* </div>
            </div> */}
          </div>
          <div className="copyright-section pt-3 pb-3">
            {/* <div className="container">
              <div className="row"> */}
                <div className="col-12 mt-3">
                  <div className="d-flex flex-row justify-content-center">
                    <p className="copyright-section-para">Copyright 2015 -</p>
                    <p className="copyright-section-para">
                      2020, Kirekou All Rights Reserved. Kirekou is a Nidrosoft
                      LLC Company.
                    </p>
                  </div>
                </div>
              </div>
            {/* // </div>
        //   </div>
        // </div>
      //  </div> */}
      </>
    );
  }
}
export default Footer;

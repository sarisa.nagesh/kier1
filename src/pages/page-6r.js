import react from 'react';
import "./page-6r.css";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";


class page6r extends react.Component {

  render() {
    return (
<>
<div className="last-23-right-section pt-5 pb-5">
    {/* <div className="container">
      <div className="row justify-content-center"> */}
        <div className="col-12 col-md-6 col-lg-7">
          <div className="shadow last-23-right-item-card mb-3">
            <div className="head-card p-3">
              <h1 className="head-card-heading">Notification Settings</h1>
            </div>
            <div className="body-card p-3">
              <h1 className="body-card-heading">Messages</h1>
              <div>
                <h1 className="body-card-heading-1 mt-5">Desktop</h1>
                <h1 className="body-card-heading-1">Show Notifications For :</h1>
                <div className="d-flex flex-row">
                  <div className="input-group mt-3">
                    <input type="text" className="form-control" style={{borderRight: 0, borderColor: 'skyblue'}} placeholder="All Activity" aria-label="All Activity" />
                    <div className="input-group-prepend">
                      <button className="btn btn-outline-secondary dropdown-toggle" style={{borderLeft: 0, borderColor: 'skyblue', color: '#fe885d'}} type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" />
                    </div>
                  </div>
                  <input className="check mt-3 ml-3" type="checkbox" />
                  <p className="body-card-para-1 mt-3 ml-3">Also Play A Sound</p>
                </div>
                <h1 className="body-card-heading-1 mt-3">Increment Message Counter For :</h1>
                <div className="input-group mt-3">
                  <input type="text" className="form-control" style={{borderRight: 0, borderColor: 'skyblue'}} placeholder="All Activity" aria-label="All Activity" />
                  <div className="input-group-prepend">
                    <button className="btn btn-outline-secondary dropdown-toggle" style={{borderLeft: 0, borderColor: 'skyblue', color: '#fe885d'}} type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" />
                  </div>
                </div>
              </div>
              <div>
                <h1 className="body-card-heading-1 mt-3">Mobile</h1>
                <h1 className="body-card-heading-1">Show Notifications For :</h1>
                <div className="d-flex flex-row">
                  <div className="input-group mt-3">
                    <input type="text" className="form-control" style={{borderRight: 0, borderColor: 'skyblue'}} placeholder="All Activity" aria-label="All Activity" />
                    <div className="input-group-prepend">
                      <button className="btn btn-outline-secondary dropdown-toggle" style={{borderLeft: 0, borderColor: 'skyblue', color: '#fe885d'}} type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" />
                    </div>
                  </div>
                  <input className="check mt-3 ml-3" type="checkbox" />
                  <p className="body-card-para-1 mt-3 ml-3">Also Play A Sound</p>
                </div>
                <h1 className="body-card-heading-1 mt-3">Increment Message Counter For :</h1>
                <div className="input-group mt-3">
                  <input type="text" className="form-control" style={{borderRight: 0, borderColor: 'skyblue'}} placeholder="All Activity" aria-label="All Activity" />
                  <div className="input-group-prepend">
                    <button className="btn btn-outline-secondary dropdown-toggle" style={{borderLeft: 0, borderColor: 'skyblue', color: '#fe885d'}} type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" />
                  </div>
                </div> 
              </div>
              <div>
                <h1 className="body-card-heading-1 mt-3">Email</h1>
                <h1 className="body-card-para-1">( Sending To F*****ss@gmil,com)</h1>
                <h1 className="body-card-heading-1 mt-3">Send An Email With Unread Activity For :</h1>
                <div className="d-flex flex-row">
                  <div className="input-group mt-3">
                    <input type="text" className="form-control" style={{borderRight: 0, borderColor: 'skyblue'}} placeholder="All Activity" aria-label="All Activity" />
                    <div className="input-group-prepend">
                      <button className="btn btn-outline-secondary dropdown-toggle" style={{borderLeft: 0, borderColor: 'skyblue', color: '#fe885d'}} type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" />
                    </div>
                  </div>
                  <input className="check mt-3 ml-3" type="checkbox" />
                  <p className="body-card-para-1 mt-3 ml-3">Also Play A Sound</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      {/* </div>
    </div> */}
  </div>
</>
)
}
}
export default page6r;
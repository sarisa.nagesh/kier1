import react from 'react';
import "./last-left.css";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";


class lastl extends react.Component {

  render() {
    return (
        <>
  <div className="last-left-section pt-5 pb-5">
    {/* <div className="container"> */}
      {/* <div className="row justify-content-center"> */}
        <div className="col-12 col-md-6 col-lg-3">
          <div className="shadow left-item-card p-3 mb-3">
            <div className="group d-flex flex-row">
              <i className="fas fa-pencil-alt" />
              <label htmlFor="xd">Title<input className="check" type="checkbox" /></label>
            </div>
            <div className="group d-flex flex-row">
              <i className="far fa-clipboard" />
              <label htmlFor="xd">Description<input className="check" type="checkbox" /></label>
            </div>
            <div className="group d-flex flex-row">
              <i className="fas fa-calendar-week" />
              <label htmlFor="xd">Details<input className="check" type="checkbox" /></label>
            </div>
            <div className="group d-flex flex-row">
              <i className="fas fa-dumbbell" />
              <label htmlFor="xd">Expertise<input className="check" type="checkbox" /></label>
            </div>
            <div className="group d-flex flex-row">
              <i className="fas fa-map-marker-alt" />
              <label htmlFor="xd">Location<input className="check" type="checkbox" /></label>
            </div>
            <div className="group d-flex flex-row">
              <i className="fas fa-eye" />
              <label htmlFor="xd">Visability<input className="check" type="checkbox" /></label>
            </div>
            <div className="group d-flex flex-row">
              <i className="fas fa-hand-holding-usd" />
              <label htmlFor="xd">Budget<input className="check" type="checkbox" /></label>
            </div>
            <div className="group d-flex flex-row">
              <i className="fas fa-check" />
              <label htmlFor="xd">Review<input className="check" type="checkbox" /></label>
            </div>
          </div>
        </div>
      </div>
    {/* </div> */}
  {/* </div> */}
</>

    )
  }
}
export default lastl;
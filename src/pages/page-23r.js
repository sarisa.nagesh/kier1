import react from 'react';
import "./page-23r.css";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";


class page23r extends react.Component {

  render() {
    return (
<>
<div className="last-23-right-section pt-5 pb-5">
    {/* <div className="container"> */}
      {/* <div className="row justify-content-center"> */}
        <div className="col-12 col-md-6 col-lg-7">
          <div className="shadow last-23-right-item-card mb-3">
            <div className="head-card p-3">
              <div className="d-flex flex-row">
                <h1 className="head-card-heading">Expertise</h1>
                <h1 className="head-card-heading-1">Step 4 Of 8</h1>
                <h1 className="head-card-heading-2 ml-auto">Saved</h1>
              </div>
            </div>
            <div className="body-card p-3">
              <h1 className="body-card-heading">What Skills And Expertise Are Most Are Most Important To You In Mobile App Development?</h1>
              <h1 className="body-card-heading-1 mt-3">Devices (Optional)</h1>
              <div className="group-1 d-flex flex-row mt-3 mb-3">
                <button className="expertise">loreum epseum +</button>
                <button className="expertise">loreum epseum +</button>
                <button className="expertise">loreum epseum +</button>
                <button className="expertise">loreum epseum +</button>
                <Link className="see-more" href="#">See more</Link>
              </div>
            </div>
          </div>
        </div>
      {/* </div> */}
    {/* </div> */}
  </div>
</>
)
}
}
export default page23r;
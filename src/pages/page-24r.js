import react from 'react';
import "./page-24r.css";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";


class page24r extends react.Component {

  render() {
    return (
<>

  <div className="last-right-section pt-5 pb-5">
    {/* <div className="container"> */}
      {/* <div className="row justify-content-center"> */}
        <div className="col-12 col-md-6 col-lg-7">
          <div className="shadow menu-item-card mb-3">
            <div className="head-card p-3">
              <h1 className="head-card-heading">Getting Started</h1>
            </div>
            <div className="body-card p-3">
              <h1 className="body-card-heading">What Would You Like To Do ?</h1>
              <div className="group-1 d-flex flex-row">
                <div className="form-check">
                  <input className="form-check-input position-static" type="radio" name="blankRadio" id="blankRadio1" defaultValue="option1" aria-label="..." />
                </div>
                <p className="group-1-para">Create New Job Post</p>
              </div>
              <div className="group-1 d-flex flex-row">
                <div className="form-check">
                  <input className="form-check-input position-static" type="radio" name="blankRadio" id="blankRadio1" defaultValue="option1" aria-label="..." />
                </div>
                <p className="group-1-para">Edit An-Existing Draft</p>
              </div>
              <div className="group-1 d-flex flex-row">
                <div className="form-check">
                  <input className="form-check-input position-static" type="radio" name="blankRadio" id="blankRadio1" defaultValue="option1" aria-label="..." />
                </div>
                <p className="group-1-para">Reuse A Previous Job Post</p>
              </div>
              <div className="d-flex flex-row justify-content-center mt-5 mb-3">
                <button className="btn btn-banner ml-3" href="#" id="location-card-Item2">Back</button>
                <button className="btn btn-banner ml-3" href="#" id="location-card-Item3">Next</button>
              </div>
            </div>
          </div>
        </div>
      {/* </div> */}
    {/* </div> */}
  </div>
</>


)
}
}
export default page24r;